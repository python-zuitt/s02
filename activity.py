year = int(input("Please input a year: \n"))

if year <= 0:
	print("No zero or negative value.")
elif year%4 >= 1:
	print(f"{year} is not a leap year.")
elif year%4 == 0:
	print(f"{year} is a leap year.")
else:
	print("Strings are not allowed for input.")

row = int(input("Enter number of rows: \n"))
col = int(input("Enter number of columns: \n"))

star = ""

for x in range(col):
	star += "*"

for y in range(row):
	print(f"{star}")